# BenchMe


TP COLLE Développement 

## Pour commencer

Les algorithmes de tri permettent de trier les tableaux. <br>
Nous allons en étudier trois sortes :<br> 
- Tri a bulle <br>
- Tri par sélection <br>
- Tri par insértion <br>

A l'aide du benchmark nous allons pouvoir voir lequel est le plus efficace <br>
Les résultats seront noté dans un fichier Csv créer automatiquement

### Pré-requis

Pour pouvoir éxecuter le projet il faut : <br>

- Un IDE (De préfèrence NetBeans pour éviter des problèmes de compatibilité)

### Installation

Une fois le projet récupérer, il suffit d'ouvrir le projet à l'aide d'un IDE. <br>
Pour cela il faudra aller dans la barre du haut de notre IDE et de choisir open project puis de sélectionner le projet.


## Démarrage et résutats attendu 

Une fois le projet ouvert, on peut le lancer en appuyant sur la touche F6 ou bien en cliquant sur RUN. <br>
On devrait alors avoir un dossier CSV qui s'est généré dans lequel sera situé les résultats du BenchMark des trois algorithmes 

## Fabriqué avec

* [NetBeans](https://netbeans.apache.org/download/index.html) - IDE




## Versions

**Dernière version :** 1.1 (Ajout du ReadMe.md) <br>
**Version :** 1.0 (3 Algo fonctionnel + Csv + Benchmark +)


## Auteurs

* **Lucas Chanaux** _alias_ [@luc614fr](https://github.com/luc614fr)
* **Antoine Chavée** _alias_ [@GitAntoine](https://github.com/GitAntoine)



