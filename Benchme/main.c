/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file main.c
 * \file Antoine.C Lucas.C
 * \version 1.0
 * \date 26 Septembre
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include<time.h>
#include<string.h>

#include "benchmeCsv.h"
#include "featureTri.h"
#include "function_tableau.h"


/**
 * \brief BenchMark des trois fonctions
 * 
 * 
 */


/*
 * 
 */


 
int main(void) {
    
     /** initialisation des tableaux et des pointeurs pour recevoir les moyens des temps des fonction**/
    double temps;
    clock_t start;
    double tabTempBubble[3];
    double tabTempSelection[3];
    double tabTempInsert[3];
    
    double tabMoyenTemp[256];
    float *tableau;
    double *tableauTempBubble;
    double *tableauSelection;
    double *tableauInsert;
    
    double moyenTempBubble[3];
    double moyenTempSelection[3];
    double moyenTempInsert[3];
     /**initialisation des variables de boucles**/
    int length;
    int j;
    int i;
    /** boucle pour passer en paramètre 3 fois pour chaque fonction une taille différente**/
    for(j = 0; j < 3; j++ ){ 
        if(j==0)
        {           
            length=100;
        }
        if(j==1)
        {          
            length=1000;
        }
        if(j==2)
        {   
            length=10000;
        } 
        /** boucle pour calculer les moyennes du temps d'éxecution des fonction**/
     for ( i = 0; i < 3; i++ )
     {
    float newTab[length];  
    tableauAleatoire(newTab,length);
    tableau = newTab;
    start = clock(); //enclenche le détonateur
    bubble_sorting(tableau,length); 
    temps = (double)(clock()-start)/(double)CLOCKS_PER_SEC;
    tabTempBubble[i]=temps;
    
    tableauAleatoire(newTab,length);
    tableau = newTab;
    
    start = clock(); //enclenche le détonateur
    sort_by_selection(tableau,length); 
    temps = (double)(clock()-start)/(double)CLOCKS_PER_SEC;
    tabTempSelection[i]=temps;
    
    tableauAleatoire(newTab,length);
    tableau = newTab;
    
    
    start = clock(); //enclenche le détonateur
    tri_insert(tableau,length); 
    temps = (double)(clock()-start)/(double)CLOCKS_PER_SEC;
    tabTempInsert[i]=temps;
    
    
    
    }
         /** calcule de la moyenne et affectation dans un tableaux**/
     moyenTempBubble[j] = (tabTempBubble[0]+tabTempBubble[1]+ tabTempBubble[2])/3;
     moyenTempSelection[j] = (tabTempSelection[0]+tabTempSelection[1]+ tabTempSelection[2])/3;
     moyenTempInsert[j] = (tabTempInsert[0]+tabTempInsert[1]+ tabTempInsert[2])/3;

    }    
    /** affectation des pointeurs  **/
    tableauTempBubble = moyenTempBubble;
    tableauSelection = moyenTempSelection;
    tableauInsert = moyenTempInsert;
    
     /** affichage des temps moyens**/
     for ( i = 0; i < 3; i++ ){
     printf("*(tableau + %d) : %f\n",  i, *(tableauTempBubble + i) );
   }   
    for ( i = 0; i < 3; i++ ){
     printf("*(tableau + %d) : %f\n",  i, *(tableauSelection + i) );
   }   
    for ( i = 0; i < 3; i++ ){
     printf("*(tableau + %d) : %f\n",  i, *(tableauInsert + i) );
   }
    
     /** création du fichier csv avec les resultat des moyenne de temps d'éxécution des fonctions  **/
    
   create_marks_csv(tableauTempBubble,tableauSelection,tableauInsert);

  /** @return Tableau trié **/

    return (EXIT_SUCCESS);
    
    
}

