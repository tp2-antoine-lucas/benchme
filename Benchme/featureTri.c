/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file featureTri.c
 * \file Antoine.C Lucas.C
 * \version 1.0
 * \date 26 Septembre
 * 
 */

#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

/**
 * \brief sort tab values with the bubble sorting method
 * 
 * Tri d'un tableau par méthode de tri a bulle 
 * 
 * @param pointTab
 * @param lenght
 *
 */

/* fonction permetant de trier sont tableaux du plus petit au plus grand avec la méthode a bulles*/



void bubble_sorting(float *pointTab,int lenght){   
    int i =0;
    /* Booléen marquant l'arrêt du tri si le tableau est ordonné */
    bool permut = true;
    int now;    
    /* Boucle de répétition du tri*/
    while(permut){
        permut = false;
        i++;
      
     /* Vérification des éléments du tableau en fonction de leur emplacement*/
        for(now = 0;now < lenght-i;now++){
          /* si l'élément avant est plus grand que celui d'après h*/
            if(pointTab[now]>pointTab[now+1]){
               	/*swap*/     int index = 0;
                permut = true;
                int valueTemp = pointTab[now];
                pointTab[now]=pointTab[now+1];
                pointTab[now+1]= valueTemp;
            }
        }
        
    }
   
}


/**
 * \brief sort tab values with the sort by selection method
 * 
 * Tri d'un tableau par méthode de selection 
 * @param pointTab
 * @param lenght
 */



void sort_by_selection(float *pointTab,int lenght){
    int minimum,j,temp,index;
    /* Boucle pour récupérer le minimum*/
    for( index=0;index<lenght-1;index++)
    {
         minimum = index;
         /* Boucle de répétition du tri*/
        for(j=index +1; j <lenght;j++){ 
            /* Vérification des éléments du tableau en fonction du minimum*/            
            if(pointTab[j]< pointTab[minimum]){
                minimum = j;
            }
        }
       /*swap*/ 
        temp = pointTab[index];
        pointTab[index] = pointTab[minimum];
        pointTab[minimum] = temp;
    }
}

/**
 * \brief sort tab values with the sort by insert method
 * 
 * Tri d'un tableau par méthode de tri par insertion
 * 
 * @param pointTab
 * @param lenght
 *
 */

/* fonction permetant de trier sont tableaux du plus petit au plus grand avec la méthode par insertion */
void tri_insert(float *pointTab,int lenght){
    int index;
    int j; 
    int now;   
    
      /* Boucle pour récupérer la plus petit valeur du tableaux*/
    for(index=0;index<lenght;index++){
        now =pointTab[index];
          /*swap pour chaque valeur plus petit du tableaux soit a la bonne place */
        for(j = index; pointTab[j-1]> now; j--){
            pointTab[j] = pointTab[j-1];
        }
        pointTab[j] = now;
        
    }
}



