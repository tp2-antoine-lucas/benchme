/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file benchmeCsv.c
 * \file Antoine.C 
 * \version 1.0
 * \date 26 Septembre
 * 
 */


#include <stdio.h>
#include <string.h>

/**
 Function create_marks_csv => crée un fichier csv avec les paramètre donné;
 * param => 3 pointeur tabtempbubble tabtempSelection tabtempInsertion;
 * resultat = > fichier csv avec moyen des temps;
 **/
void create_marks_csv(double *tabtempbubble,double *tabtempSelection,double *tabtempInsertion){

     /** instanciation des variables **/
    int i;
    
    FILE *fp;
    /** définie le nom du fichier **/
    char *filename = "benchmeResult.csv";
    /** nomme le nom du fichier **/

    fp = fopen(filename, "w+");
 /**  boucle pour afficher chaque moyen de temp pour une taille de tableau différente **/
 for (i = 0; i < 3; i++ ){
     fprintf(fp,"function_bubble;");
     if(i=1){fprintf(fp,"100;%f;",*tabtempbubble++);}
     if(i=2){fprintf(fp,"1000;%f;",*tabtempbubble++);}
     if(i=3){fprintf(fp,"10000;%f;",*tabtempbubble++);}  
   }  
      /** boucle pour afficher chaque moyen de temp pour une taille de tableau différente **/
    for ( i = 0; i < 3; i++ ){ 
  fprintf(fp,"\n function_selection;");
     if(i=1){fprintf(fp,"100;%f;",*tabtempSelection++);}
     if(i=2){fprintf(fp,"1000;%f;",*tabtempSelection++);}
     if(i=3){fprintf(fp,"10000;%f;",*tabtempSelection++);}

   }  
     /** boucle pour afficher chaque moyen de temp pour une taille de tableau différente **/
    for ( i = 0; i < 3; i++ ){
     fprintf(fp,"\n function_insert");
     if(i=1){fprintf(fp,"100;%f;",*tabtempInsertion++);}
     if(i=2){fprintf(fp,"1000;%f;",*tabtempInsertion++);}
     if(i=3){fprintf(fp,"10000;%f;",*tabtempInsertion++);}       
   }   
    /**fermeture de l'écriture du fichier **/
    fclose(fp);

    printf("File created !");
}